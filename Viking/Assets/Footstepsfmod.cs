﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Invector.vCharacterController;

public class Footstepsfmod : MonoBehaviour
{
    [FMODUnity.EventRef] public string footstepEvet;
    [FMODUnity.EventRef] public string runEvet;

    public vThirdPersonInput tpInput;
    public vThirdPersonController tpController;
    // Start is called before the first frame update
    void Start()
    {
        tpInput = GetComponent<vThirdPersonInput>();
        tpController = GetComponent<vThirdPersonController>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void footstep()
    {

        if(tpInput.cc.inputMagnitude > 0.1)
        {
            if (tpController.isSprinting)
            {
                FMODUnity.RuntimeManager.PlayOneShotAttached(runEvet, gameObject);

            }
            else 

                FMODUnity.RuntimeManager.PlayOneShotAttached(footstepEvet, gameObject);

        }
    }
}
