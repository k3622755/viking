﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tourchSoundEvent : MonoBehaviour
{
    [FMODUnity.EventRef] public string torchevent;
    private FMOD.Studio.EventInstance TorchInstance;

    // Start is called before the first frame update
    void Start()
    {
        //  FMODUnity.RuntimeManager.PlayOneShot(torchevent, gameObject.transform.position);
        TorchInstance = FMODUnity.RuntimeManager.CreateInstance(torchevent);
        TorchInstance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(gameObject));
        TorchInstance.start();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
